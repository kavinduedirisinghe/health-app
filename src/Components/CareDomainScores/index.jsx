import React, { useState, useEffect } from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import Paper from '@mui/material/Paper';

const CareDomainScores = (props) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const dataArray = [
      {
        "score": "1",
        "Fast Access": props.data.filter(item => item.careDomainScores['fastAccess'] === 1).length,
        "Effective Treatment": props.data.filter(item => item.careDomainScores['effectiveTreatment'] === 1).length,
        "Continuity Of Care": props.data.filter(item => item.careDomainScores['continuityOfCare'] === 1).length,
        "Communication And Involvement": props.data.filter(item => item.careDomainScores['communicationAndInvolvement'] === 1).length,
        "Emotional Support": props.data.filter(item => item.careDomainScores['emotionalSupport'] === 1).length,
        "Access To Physical And Emotional Needs": props.data.filter(item => item.careDomainScores['accessToPhysicalAndEmotionalNeeds'] === 1).length,
        "Billing And Admin": props.data.filter(item => item.careDomainScores['billingAndAdmin'] === 1).length,
      },
      {
        "score": "2",
        "Fast Access": props.data.filter(item => item.careDomainScores['fastAccess'] === 2).length,
        "Effective Treatment": props.data.filter(item => item.careDomainScores['effectiveTreatment'] === 2).length,
        "Continuity Of Care": props.data.filter(item => item.careDomainScores['continuityOfCare'] === 2).length,
        "Communication And Involvement": props.data.filter(item => item.careDomainScores['communicationAndInvolvement'] === 2).length,
        "Emotional Support": props.data.filter(item => item.careDomainScores['emotionalSupport'] === 2).length,
        "Access To Physical And Emotional Needs": props.data.filter(item => item.careDomainScores['accessToPhysicalAndEmotionalNeeds'] === 2).length,
        "Billing And Admin": props.data.filter(item => item.careDomainScores['billingAndAdmin'] === 2).length,
      },
      {
        "score": "3",
        "Fast Access": props.data.filter(item => item.careDomainScores['fastAccess'] === 3).length,
        "Effective Treatment": props.data.filter(item => item.careDomainScores['effectiveTreatment'] === 3).length,
        "Continuity Of Care": props.data.filter(item => item.careDomainScores['continuityOfCare'] === 3).length,
        "Communication And Involvement": props.data.filter(item => item.careDomainScores['communicationAndInvolvement'] === 3).length,
        "Emotional Support": props.data.filter(item => item.careDomainScores['emotionalSupport'] === 3).length,
        "Access To Physical And Emotional Needs": props.data.filter(item => item.careDomainScores['accessToPhysicalAndEmotionalNeeds'] === 3).length,
        "Billing And Admin": props.data.filter(item => item.careDomainScores['billingAndAdmin'] === 3).length,
      },
      {
        "score": "4",
        "Fast Access": props.data.filter(item => item.careDomainScores['fastAccess'] === 4).length,
        "Effective Treatment": props.data.filter(item => item.careDomainScores['effectiveTreatment'] === 4).length,
        "Continuity Of Care": props.data.filter(item => item.careDomainScores['continuityOfCare'] === 4).length,
        "Communication And Involvement": props.data.filter(item => item.careDomainScores['communicationAndInvolvement'] === 4).length,
        "Emotional Support": props.data.filter(item => item.careDomainScores['emotionalSupport'] === 4).length,
        "Access To Physical And Emotional Needs": props.data.filter(item => item.careDomainScores['accessToPhysicalAndEmotionalNeeds'] === 4).length,
        "Billing And Admin": props.data.filter(item => item.careDomainScores['billingAndAdmin'] === 4).length,
      },
      {
        "score": "5",
        "Fast Access": props.data.filter(item => item.careDomainScores['fastAccess'] === 5).length,
        "Effective Treatment": props.data.filter(item => item.careDomainScores['effectiveTreatment'] === 5).length,
        "Continuity Of Care": props.data.filter(item => item.careDomainScores['continuityOfCare'] === 5).length,
        "Communication And Involvement": props.data.filter(item => item.careDomainScores['communicationAndInvolvement'] === 5).length,
        "Emotional Support": props.data.filter(item => item.careDomainScores['emotionalSupport'] === 5).length,
        "Access To Physical And Emotional Needs": props.data.filter(item => item.careDomainScores['accessToPhysicalAndEmotionalNeeds'] === 5).length,
        "Billing And Admin": props.data.filter(item => item.careDomainScores['billingAndAdmin'] === 5).length,
      },

    ];
    setData(dataArray);
  }, [props]);

  return (
    <Paper className="graph-paper">
      <div className="graph-title"><h3>Care Domain Scores Comparison</h3></div>
      
      <div className="full-content-div div-reverse">
        <div className="desc-div">
          <h3>What this means ?</h3>
          <p align="justify">This multiple bar chart compares the scores for seven different care domains in the user experience. The chart displays five bars, each representing a score between 1-5 for the care domain. The bars are divided into seven segments representing the seven care domains: (1) communication, (2) responsiveness, (3) care coordination, (4) access to care, (5) patient education, (6) emotional support, and (7) overall experience. The data can be filtered by date range and source, allowing to gain insights into which care domains are performing well and where improvements can be made. The chart provides an interactive way to analyze the user experience in different domains and help healthcare providers focus on areas that require improvement.</p>
        </div>
        <div className="graph-div">          
          <div>
            <ResponsiveContainer width={'99%'} height={300} style={{textAlign: 'center'}}>
              {props.loadingStatus ?
                <div style={{textAlign: 'center'}}>loading...</div>
                :
                <BarChart width={730} height={250} data={data}>
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="score" />
                  <YAxis />
                  <Tooltip />
                  <Legend layout="horizontal" iconSize="10" />
                  <Bar dataKey="Fast Access" fill="#8884d8" />
                  <Bar dataKey="Effective Treatment" fill="#82ca9d" />
                  <Bar dataKey="Continuity Of Care" fill="#e67a73" />
                  <Bar dataKey="Communication And Involvement" fill="#0088FE" />
                  <Bar dataKey="Emotional Support" fill="#00C49F" />
                  <Bar dataKey="Access To Physical And Emotional Needs" fill="#FFBB28" />
                  <Bar dataKey="Billing And Admin" fill="#FF8042" />
                </BarChart>
              }
            </ResponsiveContainer>
          </div>
        </div>        
      </div>
    </Paper>
  );
};

export default CareDomainScores;