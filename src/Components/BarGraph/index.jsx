import React, { useState, useEffect } from "react";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const BarGraph = (props) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const dataArray = [
      {
        "Care Domain": "Fast Access",
        "Total Score": props.fastAccessTotal,
      },
      {
        "Care Domain": "Effective Treatment",
        "Total Score": props.effectiveTreatmentTotal,
      },
      {
        "Care Domain": "Continuity Of Care",
        "Total Score": props.continuityOfCareTotal,
      },
      {
        "Care Domain": "C ommunication And Involvement",
        "Total Score": props.communicationAndInvolvementTotal,
      },
      {
        "Care Domain": "Emotional Support",
        "Total Score": props.emotionalSupportTotal,
      },
      {
        "Care Domain": "Access To Physical And Emotional Needs",
        "Total Score": props.accessToPhysicalAndEmotionalNeedsTotal,
      },
      {
        "Care Domain": "Billing And Admin",
        "Total Score": props.billingAndAdminTotal,
      },

    ];
    setData(dataArray);
  }, [props]);

  return (
    <ResponsiveContainer width={'99%'} height={300} style={{textAlign: 'center'}}>
      {props.loading ?
        <div style={{textAlign: 'center'}}>loading...</div>
        :
        <BarChart
          width={730}
          height={250}
          data={data}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="Care Domain" tick={{fontSize: 12}} />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="Total Score" fill="#8884d8" />
        </BarChart>
      }
    </ResponsiveContainer>
  );
};

export default BarGraph;