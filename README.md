# Healthcare Experience Vizualiser

## Overview

Healthcare Experience Vizualiser is a web application that uses a mock API to visualize relationships between comments, sources, and scores related to healthcare experiences. 

The web app allows non-technical users to explore trends in user experience by providing interactive visualizations of the data.

## Getting started

To get started with this project, follow the steps below.

### Clone the repository

Clone this repository to your local machine using the following command:

git clone <repository-url>

### Install dependencies

Navigate to the repository and install the project's dependencies using the following command:

npm install

### Start the project

To start the project, run the following command:

npm start


## Process

To develop Healthcare Experience Visualizer, I started by researching various visualisation solutions that would allow me to create meaningful insights from the API data. I then created a React-based web application using create-react-app as a starting point.

Next, I integrated the API to retrieve the comments data and store it in the application's store as a global state. I used React hooks like useState and useEffect to manage the state of the app and update the UI based on user interactions. Also, I used react toolkit to maintain an efficient redux development throughout the project.

For the visualizations, I used the React Recharts library to create interactive charts and graphs that allow users to explore the data in different ways. I used material-UI for styling. I created a date range filter, frequency, and data source filter using React components and passed them down as props to the charts. Following are the visualisations built on the platform.

*Wordcloud of most frequent words in the comments: 
react-wordcloud library was used. As a preprocessing step, all the stop words are removed from the comments. Since the comments are not in English I removed a few sample words. But the developer can update the stop words array anytime. Also, we can use a library to get stop words.

*Piechart representing the distribution of data sources

*Line chart representing the overall comment scores varying by date
Users can select a frequency (daily, monthly, or annually), the source, the start date, and the end date. Here, the overall comments score which is less than 3 is considered as negative comments, and the comments which have a score greater than or equal to 3 are considered as positive comments. 

*Multiple bar chart to compare the scores for seven different care domains

*Bar chart illustrating the breakdown of different care-domain scores


Throughout the development process, I used Git to track my changes and commit progress regularly. 

## Design Choices

I chose to use React and Recharts because they are popular and well-supported libraries that provide a good balance of flexibility and ease of use. I also chose to use a minimalist design with a clean and simple interface, so that the focus remains on the data and visualizations.

All the date fields are validated such that date figures are disabled for future dates.

## Challenges

One of the biggest challenges I faced was dealing with the large amount of data returned by the API. So I decided not to call the API again and again to fetch the data. I stored data in the global state and filtered it from there for different purposes. But I created a graph by calling API with query params to get the filtered data to demonstrate familiarity with the URL params and filtering data with API. I had to make sure the charts were responsive and could handle different screen sizes and resolutions. 
Since all the overall scores in the dataset are less than 3, there is no data to visualise the line graph for the positive feedback properly.
Another challenge was ensuring that the app was user-friendly and accessible to non-technical users. This required proper documentation. Furthermore, the challenge of addressing these issues within a restricted time frame was also present.
## API Suggestions

I suggest adding paginations (Skip and limits) for the given mock API. Therefore, It will be more supportive for frontend developers to deal with large amounts of data and it will increase the performance of the app. I would also suggest adding more filtering options, 
such as filtering by specific care domains. Most of the time, the analysis needed only the id and scores, therefore it is better to have a different API to fetch data without comments.
