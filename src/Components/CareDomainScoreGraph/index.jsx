import React, { useState, useEffect } from "react";
import { useDispatch, useSelector} from "react-redux";
import { fetchCareDomainGraphData } from '../../features/commentsSlice';
import BarGraph from '../BarGraph';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Paper from '@mui/material/Paper';

const CareDomainScoreGraph = (props) => {
  const dispatch = useDispatch();
  let fetchedComments = useSelector((state) => state.comments.careDomainGraphData);
  let loadingStatus = useSelector((state) => state.comments.loadingCareDomainGraphData);
  const [source, setSource] = useState('facebook,twitter,google');
  const [loading, setLoading] = useState(loadingStatus);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [fastAccessTotal, setFastAccessTotal] = useState(0);
  const [effectiveTreatmentTotal, setEffectiveTreatmentTotal] = useState(0);
  const [continuityOfCareTotal, setContinuityOfCare] = useState(0);
  const [communicationAndInvolvementTotal, setCommunicationAndInvolvementTotal] = useState(0);
  const [emotionalSupportTotal, setEmotionalSupportTotal] = useState(0);
  const [accessToPhysicalAndEmotionalNeedsTotal, setAccessToPhysicalAndEmotionalNeedsTotal] = useState(0);
  const [billingAndAdminTotal, setBillingAndAdminTotal] = useState(0);

  useEffect(() => {
    setLoading(loadingStatus);
  }, [loadingStatus]);

  //fetch data from the api
  useEffect(() => {
    processTotalCommentsData();
  }, [fetchedComments]);

  useEffect(() => {
    let start = startDate ? startDate.toISOString() : null;
    let end = endDate ? endDate.toISOString() : null;
    dispatch(fetchCareDomainGraphData({source, start, end}));
  }, [dispatch, source, startDate, endDate]);

  const sumCareDomainTitle = (data, title) => {
    const sum = data.reduce((acc, obj) => {
      if (obj.careDomainScores[title] !== null && obj.careDomainScores[title] !== undefined) {
        return acc + obj.careDomainScores[title];
      }
      return acc;
    }, 0);
    if (title === 'fastAccess') {
      setFastAccessTotal(sum);
    } else if (title === 'effectiveTreatment') {
      setEffectiveTreatmentTotal(sum);
    } else if (title === 'continuityOfCare') {
      setContinuityOfCare(sum);
    } else if (title === 'communicationAndInvolvement') {
      setCommunicationAndInvolvementTotal(sum);
    } else if (title === 'emotionalSupport') {
      setEmotionalSupportTotal(sum);
    } else if (title === 'accessToPhysicalAndEmotionalNeeds') {
      setAccessToPhysicalAndEmotionalNeedsTotal(sum);
    } else if (title === 'billingAndAdmin') {
      setBillingAndAdminTotal(sum);
    }
  }

  const processTotalCommentsData = () => {
    sumCareDomainTitle(fetchedComments, 'fastAccess');
    sumCareDomainTitle(fetchedComments, 'effectiveTreatment');
    sumCareDomainTitle(fetchedComments, 'continuityOfCare');
    sumCareDomainTitle(fetchedComments, 'communicationAndInvolvement');
    sumCareDomainTitle(fetchedComments, 'emotionalSupport');
    sumCareDomainTitle(fetchedComments, 'accessToPhysicalAndEmotionalNeeds');
    sumCareDomainTitle(fetchedComments, 'billingAndAdmin');
  }

  const handleChange = (event) => {
    setSource(event.target.value);
  }

  const handleStartDateSelect = (value) => {
    setStartDate(value);
  }

  const handleEndDateSelect = (value) => {
    setEndDate(value);
  }
  
  return (
    <Paper className="graph-paper">
      <div className="graph-title"><h3>Patient Experience by Care Domain</h3></div>
      <div className="selection-div">
      <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
          <InputLabel id="demo-select-small-label">Source</InputLabel>
          <Select
            name="source"
            value={source}
            label="Source"
            onChange={handleChange}
          >
            <MenuItem value='facebook,twitter,google'>All</MenuItem>
            <MenuItem value='google'>Google</MenuItem>
            <MenuItem value='twitter'>Twitter</MenuItem>
            <MenuItem value='facebook'>Facebook</MenuItem>
          </Select>
        </FormControl>
        <FormControl>
          <DatePicker
            placeholderText="Start Date"
            selected={startDate}
            onSelect={handleStartDateSelect} //when day is clicked
            className="date-picker-custom-class"
            maxDate={endDate ? endDate : new Date()}
          />
        </FormControl>
        <FormControl>
          <DatePicker
            placeholderText="End Date"
            selected={endDate}
            onSelect={handleEndDateSelect} //when day is clicked
            className="date-picker-custom-class"
            maxDate={new Date()}
          />
        </FormControl>
      </div>
      <div className="full-content-div">
        <div className="graph-div">          
          <div>
            <BarGraph
              fastAccessTotal = {fastAccessTotal}
              effectiveTreatmentTotal={effectiveTreatmentTotal}
              continuityOfCareTotal={continuityOfCareTotal}
              communicationAndInvolvementTotal={communicationAndInvolvementTotal}
              emotionalSupportTotal={emotionalSupportTotal}
              accessToPhysicalAndEmotionalNeedsTotal={accessToPhysicalAndEmotionalNeedsTotal}
              billingAndAdminTotal={billingAndAdminTotal}
              loading={loadingStatus}
            />
          </div>
        </div>
        <div className="desc-div">
          <h3>What this means ?</h3>
          <p align="justify">The bar chart visualizes user feedback across the seven care domains. Each bar represents a specific domain, and the height of the bar indicates the total count of feedback received for that domain. Hover over each bar to see the exact count. This visualization helps identify which care domains are receiving the most feedback and can guide healthcare providers in focusing their efforts for improving patient experience.</p>
        </div>
      </div>
    </Paper>
  );
};
  
export default CareDomainScoreGraph;