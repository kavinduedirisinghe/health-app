import React, { useState, useEffect } from "react";
import { useDispatch, useSelector} from "react-redux";
import { fetchComments } from '../../features/commentsSlice';
import WordCloudView from "../../Components/WordCloudView";
import SourceDistribution from "../../Components/SourceDistribution";
import TotalCommentsGraph from '../../Components/TotalCommentsGraph';
import CareDomainScores from "../../Components/CareDomainScores";

import CareDomainScoreGraph from "../../Components/CareDomainScoreGraph";
// import CorrelationHeatMap from "../../Components/CorrelationHeatMap";
import "../../styles/dashboard.css";

const Dashboard = () => {

  const dispatch = useDispatch();
  let fetchedComments = useSelector((state) => state.comments.comments);
  let loadingStatus = useSelector((state) => state.comments.loading);

  useEffect(() => {
    dispatch(fetchComments());
  }, [dispatch]);

  return (
    <div>
      <h2 className="app-title">Healthcare Experience Visualiser</h2>
      <WordCloudView data={fetchedComments} loadingStatus={loadingStatus} />
      <SourceDistribution data={fetchedComments} loadingStatus={loadingStatus} />
      <TotalCommentsGraph data={fetchedComments} loadingStatus={loadingStatus} />
      <CareDomainScores data={fetchedComments} loadingStatus={loadingStatus} />
      <CareDomainScoreGraph />

      {/* 
      
      <CorrelationHeatMap data={fetchedComments} oadingStatus={loadingStatus}/> */}
    </div>
  );
};
  
export default Dashboard;
  