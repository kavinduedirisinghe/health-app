import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  loading: false,
  error: "",
  comments: [],
  careDomainGraphData: [],
  loadingCareDomainGraphData: false,
};

export const fetchComments = createAsyncThunk("comment/fetchComments", () => {
  return axios
    .get(`https://77ke2s5tn7gprqjtu2kcldaaru0uarhb.lambda-url.eu-west-2.on.aws`, 
    {
      headers: {},
    })
      .then((res) => {
        return res.data; 
      }
    )
});

export const fetchCareDomainGraphData = createAsyncThunk("comment/fetchCareDomainGraphData", (params) => {
  let queryParams = `sources=${params.source}`;
  if (params.start) {
    queryParams = queryParams+'&fromDate='+params.start;
  }
  if (params.end) {
    queryParams = queryParams+'&toDate='+params.end;
  }
  return axios
    .get(`https://77ke2s5tn7gprqjtu2kcldaaru0uarhb.lambda-url.eu-west-2.on.aws?${queryParams}`, 
    {
      headers: {},
    })
      .then((res) => {
        return res.data; 
      }
    )
});

const commentsSlice = createSlice({
  name: "comments",
  initialState,
  extraReducers: (builder) => {

    //Fetch comments
    builder.addCase(fetchComments.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetchComments.fulfilled, (state, action) => {
      state.loading = false;
      state.comments = action.payload;
      state.error = "";
    });
    builder.addCase(fetchComments.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });

    //fetch data with filters
    builder.addCase(fetchCareDomainGraphData.pending, (state) => {
      state.loadingCareDomainGraphData = true;
    });
    builder.addCase(fetchCareDomainGraphData.fulfilled, (state, action) => {
      state.loadingCareDomainGraphData = false;
      state.careDomainGraphData = action.payload;
      state.error = "";
    });
    builder.addCase(fetchCareDomainGraphData.rejected, (state, action) => {
      state.loadingCareDomainGraphData = false;
      state.error = action.error.message;
    });
  },
});
  
//reducer
export default commentsSlice.reducer;

