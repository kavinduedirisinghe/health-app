import React, { useState, useEffect } from "react";
import LineGraph from '../LineGraph';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import moment from 'moment';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Paper from '@mui/material/Paper';

const TotalCommentsGraph = (props) => {
  const [source, setSource] = useState('all');
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [frequency, setFrequency] = useState('monthly');
  const [negativeDataSet, setNegativeDataSet] = useState([]);
  const [positiveDataSet, setPositiveDataSet] = useState([]);

  useEffect(() => {
    if (props.data) {
      filterData();
    }
  }, [props.data]);

  useEffect(() => {
    filterData();
  }, [source, startDate, endDate, frequency]);

  const filterData = () => {
    let filteredData = props.data;
    if (source !=='all' && startDate && endDate) {
      filteredData = props.data.filter(
        item =>
          item.source === source &&
          item.postedDate >= startDate.toISOString() &&
          item.postedDate <= endDate.toISOString()
      );
    } else if (source !=='all' && startDate) {
      filteredData = props.data.filter(
        item =>
          item.source === source &&
          item.postedDate >= startDate.toISOString()
      );
    } else if (source !=='all' && endDate) {
      filteredData = props.data.filter(
        item =>
          item.source === source &&
          item.postedDate <= endDate.toISOString()
      );
    } else if (startDate && endDate) {
      filteredData = props.data.filter(
        item =>
        item.postedDate >= startDate.toISOString() &&
        item.postedDate <= endDate.toISOString()
      );
    } else if (source !=='all') {
      filteredData = props.data.filter(
        item =>
          item.source === source
      );
    } else if (startDate) {
      filteredData = props.data.filter(
        item =>
        item.postedDate >= startDate.toISOString()
      );
    } else if (endDate) {
      filteredData = props.data.filter(
        item =>
        item.postedDate <= endDate.toISOString()
      );
    }

    if (filteredData) {
      let dateFormat = "YYYY-MM-DD";
      if (frequency === 'monthly') {
        dateFormat = "YYYY-MM";
      } else if (frequency === 'yearly') {
        dateFormat = "YYYY";
      }
      let dataSet = filteredData.map(v => ({value: v.overallRecommendScore, date: moment(v.postedDate).format(dateFormat), result: v.overallRecommendScore > 2 ? 'positive' : 'negative'}))
      const negativeCountByDate = dataSet.reduce((acc, curr) => {
        const dateObj = acc.find(item => item.date === curr.date);
        if (dateObj) {
          dateObj.total++;
          if (curr.result === 'negative') {
            dateObj.value++;
          }
        } else {
          acc.push({
            date: curr.date,
            total: 1,
            value: curr.result === 'negative' ? 1 : 0
          });
        }
        return acc;
      }, []);
      setNegativeDataSet(negativeCountByDate);

      const positiveCountByDate = dataSet.reduce((acc, curr) => {
        const dateObj = acc.find(item => item.date === curr.date);
        if (dateObj) {
          dateObj.total++;
          if (curr.result === 'positive') {
            dateObj.value++;
          }
        } else {
          acc.push({
            date: curr.date,
            total: 1,
            value: curr.result === 'positive' ? 1 : 0
          });
        }
        return acc;
      }, []);
      setPositiveDataSet(positiveCountByDate);
    }
  }

  const handleChange = (event) => {
    if (event.target.name === 'frequency') {
      setFrequency(event.target.value);
    } if (event.target.name === 'source') {
      setSource(event.target.value);
    }
  }

  const handleStartDateSelect = (value) => {
    setStartDate(value);
  }

  const handleEndDateSelect = (value) => {
    setEndDate(value);
  }
  
  return (
    <Paper className="graph-paper">
      <div className="graph-title"><h3>User Feedback Trend</h3></div>
      <div className="selection-div">
        <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
          <InputLabel>Frequency</InputLabel>
          <Select
            name="frequency"
            value={frequency}
            label="Frequency"
            onChange={handleChange}
          >
            <MenuItem value='daily'>Daily</MenuItem>
            <MenuItem value='monthly'>Monthly</MenuItem>
            <MenuItem value='yearly'>Yearly</MenuItem>
          </Select>
        </FormControl>
        <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
          <InputLabel>Source</InputLabel>
          <Select
            name="source"
            value={source}
            label="Source"
            onChange={handleChange}
          >
            <MenuItem value='all'>All</MenuItem>
            <MenuItem value='google'>Google</MenuItem>
            <MenuItem value='twitter'>Twitter</MenuItem>
            <MenuItem value='facebook'>Facebook</MenuItem>
          </Select>
        </FormControl>
        <FormControl>
          <DatePicker
            placeholderText="Start Date"
            selected={startDate}
            onSelect={handleStartDateSelect} //when day is clicked
            className="date-picker-custom-class"
            maxDate={endDate ? endDate : new Date()}
          />
        </FormControl>
        <FormControl>
          <DatePicker
            placeholderText="End Date"
            selected={endDate}
            onSelect={handleEndDateSelect} //when day is clicked
            className="date-picker-custom-class"
            maxDate={new Date()}
          />
        </FormControl>
      </div>
      <div className="">
        <div className="">          
          <div className="full-content-div">
            <div className="graph-div">
              <LineGraph title="Visualising Negative Feedback Over Time" data = {negativeDataSet} loading={props.loadingStatus}/>
            </div>
            <div className="desc-div">
              <h3>What this means ?</h3>
              <p align="justify">Track user feedback trends over time with the interactive line chart! This chart displays two plots - one for the count of negative feedback and one for the total feedback over the period. You can filter the frequency of the data to be displayed as monthly, daily, or annually, and filter by source (Google, Facebook, or Twitter). By specifying a start and end date, you can zoom in on specific time periods and explore how feedback trends have evolved over time. Hover over a point to see the exact count of negative feedback or total feedback for a specific time and use the chart to identify trends in feedback.</p>
            </div>
          </div>
          <div className="full-content-div">
            <div className="graph-div">
              <LineGraph title="Visualising Positive Feedback Over Time" data = {positiveDataSet} loading={props.loadingStatus}/>
            </div>
            <div className="desc-div">
              <h3>What this means ?</h3>
              <p align="justify">To explore positive feedback trends, follow the same steps as for the negative feedback plot. Use the filter options to narrow down the data by frequency, source, and date range. Compare the positive and negative feedback plots side by side to get a comprehensive view of overall feedback trends.</p>
            </div>
          </div>
        </div>
      </div>
    </Paper>
  );
};
  
export default TotalCommentsGraph;
  