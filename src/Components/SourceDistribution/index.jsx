import React, { useState, useEffect } from "react";
import "react-datepicker/dist/react-datepicker.css";
import { PieChart, Pie, Sector, ResponsiveContainer, Cell, Legend } from 'recharts';
import Paper from '@mui/material/Paper';

const SourceDistribution = (props) => {
  const [distribution, setDistribution] = useState([]);
  const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
  const RADIAN = Math.PI / 180;
  
  const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
        <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
            {`${(percent * 100).toFixed(0)}%`}
        </text>
    );
  };

  useEffect(() => {
    if (props.data) {
      processData();
    }
  }, [props.data]);

  const processData = () => {
    console.log(props.data)
    const countBySource = props.data.reduce((acc, curr) => {
      const sourceObj = acc.find((obj) => obj.name === curr.source);
      if (sourceObj) {
        sourceObj.count++;
      } else {
        acc.push({ name: curr.source, count: 1 });
      }
      return acc;
    }, []);
    console.log(countBySource)
    setDistribution(countBySource);
  }

  return (
    <Paper className="graph-paper">
      <div className="graph-title"><h3>Visualise Data Sources with an Interactive Pie Chart</h3></div>
      <div className="full-content-div div-reverse">
        <div className="desc-div">
          <h3>What this means ?</h3>
          <p align="justify">Explore the distribution of our data sources with an interactive pie chart! Each slice represents the percentage of data coming from Google, Twitter, and Facebook, and a legend is provided to help you easily distinguish between the sources. By exploring the data sources breakdown, you can gain insights into the distribution of user comments across different platforms.</p>
        </div>
        <div className="graph-div">          
          <div>
            {props.loadingStatus ?
              <div style={{textAlign: 'center'}}>loading...</div>
              :
              <ResponsiveContainer width={'99%'} height={300} style={{textAlign: 'center'}}>
              {props.loading ?
                <div style={{textAlign: 'center'}}>loading...</div>
                :
                <PieChart width={500} height={500}>
                  <Legend layout="horizontal" verticalAlign="bottom" align="center" />
                  <Pie
                    data={distribution}
                    cx="50%"
                    cy="50%"
                    labelLine={false}
                    label={renderCustomizedLabel}
                    outerRadius={100}
                    fill="#8884d8"
                    dataKey="count"
                  >
                    {distribution.map((entry, index) => (
                        <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                    ))}
                  </Pie>
                </PieChart>
              }
            </ResponsiveContainer>
            }
          </div>
        </div>        
      </div>
    </Paper>
/*

    <div>
      <div className="graph-title"><h3>Data Sources</h3></div>
        <div>
          {props.loadingStatus ?
            <div style={{textAlign: 'center'}}>loading...</div>
            :
            <ResponsiveContainer width={'99%'} height={300} style={{textAlign: 'center'}}>
              {props.loading ?
                <div style={{textAlign: 'center'}}>loading...</div>
                :
                <PieChart width={400} height={400}>
                  <Legend layout="horizontal" verticalAlign="bottom" align="right" />
                    <Pie
                        data={distribution}
                        cx="50%"
                        cy="50%"
                        labelLine={false}
                        label={renderCustomizedLabel}
                        outerRadius={80}
                        fill="#8884d8"
                        dataKey="count"
                    >
                        {distribution.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                        ))}
                    </Pie>
                </PieChart>
              }
            </ResponsiveContainer>
          }
        </div>
    </div>*/
  );
};

export default SourceDistribution;