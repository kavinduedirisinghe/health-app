import React from "react";
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const LineGraph = (props) => {
  return (
    <>
      <h5>{props.title}</h5>
      <ResponsiveContainer width={'99%'} height={300} style={{textAlign: 'center'}}>
        {props.loading ?
          <div style={{textAlign: 'center'}}>loading...</div>
          :
          <LineChart
            // width={500}
            // height={300}
            data={props.data}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="date" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Line type="monotone" strokeDasharray="10 10" dataKey="value" stroke="red" activeDot={{ r: 1 }} dot={true} />
            <Line type="monotone" strokeDasharray="15 15" dataKey="total" stroke="#8884d8" activeDot={{ r: 1 }} dot={true} />
          </LineChart>
        }
      </ResponsiveContainer>
    </>
  );
};

export default LineGraph;