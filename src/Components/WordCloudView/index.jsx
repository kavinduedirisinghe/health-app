import React, { useState, useEffect } from "react";
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import WordCloud from 'react-wordcloud';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Paper from '@mui/material/Paper';

const WordCloudView = (props) => {
  const [source, setSource] = useState('all');
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [processedData, setProcessedData] = useState([]);
  const [wordsArray, setWordsArray] = useState([]);

  useEffect(() => {
    if (props.data) {
      setProcessedData(props.data);
      filterData();
    }
  }, [props.data]);

  useEffect(() => {
    filterData();
  }, [source, startDate, endDate]);

  const filterData = () => {
    let filteredData = props.data;
    if (source !=='all' && startDate && endDate) {
      filteredData = props.data.filter(
        item =>
          item.source === source &&
          item.postedDate >= startDate.toISOString() &&
          item.postedDate <= endDate.toISOString()
      );
    } else if (source !=='all' && startDate) {
      filteredData = props.data.filter(
        item =>
          item.source === source &&
          item.postedDate >= startDate.toISOString()
      );
    } else if (source !=='all' && endDate) {
      filteredData = props.data.filter(
        item =>
          item.source === source &&
          item.postedDate <= endDate.toISOString()
      );
    } else if (startDate && endDate) {
      filteredData = props.data.filter(
        item =>
        item.postedDate >= startDate.toISOString() &&
        item.postedDate <= endDate.toISOString()
      );
    } else if (source !=='all') {
      filteredData = props.data.filter(
        item =>
          item.source === source
      );
    } else if (startDate) {
      filteredData = props.data.filter(
        item =>
        item.postedDate >= startDate.toISOString()
      );
    } else if (endDate) {
      filteredData = props.data.filter(
        item =>
        item.postedDate <= endDate.toISOString()
      );
    }

    // These words not included in calculations
    const stopWords = ["est", "ut", "at", "eos"];
    const wordCount = {};
    if (filteredData) {
      // Loop through each comment and split the text into words
      filteredData.forEach((comment) => {
        const words = comment.comment.split(" ");
    
        // Count the frequency of each word, ignoring stop words
        words.forEach((word) => {
          if (!stopWords.includes(word)) {
            if (wordCount[word]) {
              wordCount[word]++;
            } else {
              wordCount[word] = 1;
            }
          }
        });
      });
    
      // Convert the object into an array of word objects
      const words = Object.keys(wordCount).map((word) => ({
        text: word,
        value: wordCount[word],
      }));
      setWordsArray(words);
    }
  }

  const handleChange = (event) => {
    setSource(event.target.value);
  }

  const handleStartDateSelect = (value) => {
    setStartDate(value);
  }

  const handleEndDateSelect = (value) => {
    setEndDate(value);
  }
  
  return (
    <Paper className="graph-paper">
      <div className="graph-title"><h3>User Feedback Word Cloud: Visualizing Common Themes and Topics</h3></div>
      <div className="selection-div">
        <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
          <InputLabel >Source</InputLabel>
          <Select
            name="source"
            value={source}
            label="Source"
            onChange={handleChange}
          >
            <MenuItem value='all'>All</MenuItem>
            <MenuItem value='google'>Google</MenuItem>
            <MenuItem value='twitter'>Twitter</MenuItem>
            <MenuItem value='facebook'>Facebook</MenuItem>
          </Select>
        </FormControl>
        <FormControl className="date-picker-form-div">
          <DatePicker
            placeholderText="Start Date"
            selected={startDate}
            onSelect={handleStartDateSelect} //when day is clicked
            className="date-picker-custom-class"
            maxDate={endDate ? endDate : new Date()}
          />
        </FormControl>
        <FormControl>
          <DatePicker
            placeholderText="End Date"
            selected={endDate}
            onSelect={handleEndDateSelect} //when day is clicked
            className="date-picker-custom-class"
            maxDate={new Date()}
          />
        </FormControl>
      </div>
      <div className="full-content-div">
        <div className="graph-div">          
          <div>
            {props.loadingStatus ?
              <div style={{textAlign: 'center'}}>loading...</div>
              :
              <WordCloud
                words={wordsArray}
              />
            }
          </div>
        </div>
        <div className="desc-div">
          <p align="justify">Explore the most frequent words from user comments in the interactive word cloud. The size of each word corresponds to its frequency in the comments. Hover over a word to see the exact frequency count. Use this tool to gain insights into common themes and topics that users are discussing and identify areas where you can make improvements to enhance the user experience.</p>
        </div>
      </div>
    </Paper>
  );
};

export default WordCloudView;